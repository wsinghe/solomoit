@extends('layouts/app')

@section('content')
<div class="row">
    <div class="col-md-12">
        <h2>Find Your Traveling Journey</h2>
    </div>
</div>

@include('partials/msg')

<div class="table-responsive mt-3">
    <table class="table table-striped table-sm">
        <thead>
        <tr>
            <th>#</th>
            <th>Train Name</th>
            <th>Departure Date & Time</th>
            <th>Total Seats</th>
            <th>Available Seats</th>
            <th>****</th>
        </tr>
        </thead>
        <tbody>
        @foreach($trains as $train)
            <tr>
                <td>{{ $train->id }}</td>
                <td>{{ $train->name }}</td>
                <td>{{ $train->departure_datetime }}</td>
                <td>{{ $train->seats }}</td>
                <td  class="text-success">{{ $train->available_seat }}</td>
                <td>
                    @if($train->available_seat <= 0)
                        <span class="text-danger">All seats are booked...</span>
                    @elseif (strtotime($train->departure_datetime) <= now()->timestamp)
                        <span class="text-danger">Departure Date is passed...</span>
                    @else
                        <a type="button" class="btn btn-outline-dark" href="{{ route('home.single', [$train->id]) }}">Book</a>
                    @endif
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
@endsection
