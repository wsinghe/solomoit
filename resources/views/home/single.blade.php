@extends('layouts/app')

@section('content')
<div class="row">
    <div class="col-md-12">
        <h2>Book Train - [{{ $train->name }}]</h2>
    </div>
</div>

@include('partials/msg')

<div class="table-responsive mt-3 single">
    <div class="card">
        <table class="table table-striped table-sm mb-2">
            <tbody>
                <tr>
                    <td><strong>Train Name</strong></td>
                    <td>{{ $train->name }}</td>
                </tr>
                <tr>
                    <td><strong>Departure Date & Time</strong></td>
                    <td>{{ $train->departure_datetime }}</td>
                </tr>
                <tr>
                    <td><strong>Total Seats</strong></td>
                    <td>{{ $train->seats }}</td>
                </tr>
                <tr>
                    <td><strong>Available Seats</strong></td>
                    <td class="text-success">{{ $train->available_seat }}</td>
                </tr>
            </tbody>
        </table>
        <form method="post" action="{{route('home.single', [$train->id])}}">
            <div class="card-body">
                @csrf
                <div class="form-group">
                    <label for="name">NIC</label>
                    <input type="text" id="nic" name="nic" class="form-control">
                    <input type="hidden" id="train_id" name="train_id" value="{{ $train->id }}">
                </div>
                <div class="form-group">
                    <label for="seats">Seat Count</label>
                    <input type="number" id="seat_count" name="seat_count" class="form-control">
                </div>
            </div>
            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Book and Confirm</button>
            </div>
        </form>
    </div>
</div>
@endsection
