@extends('layouts/app')

@section('content')
<div class="row">
    <div class="col-md-12">
        <h2>Create New Train</h2>
    </div>
</div>

@include('partials/msg')

<div class="table-responsive mt-3">
    <div class="card">
        <form method="post" action="{{route('train.store')}}">
            <div class="card-body">
                @csrf
                <div class="form-group">
                    <label for="name">Train Name</label>
                    <input type="text" id="name" name="name" class="form-control">
                </div>
                <div class="form-group">
                    <label for="departure_datetime">Departure Date & Time</label>
                    <input type="datetime-local" id="departure_datetime" name="departure_datetime" class="form-control">
                </div>
                <div class="form-group">
                    <label for="seats">Departure Date & Time</label>
                    <input type="number" id="seats" name="seats" class="form-control">
                </div>
            </div>
            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Create</button>
            </div>
        </form>
    </div>
</div>
@endsection
