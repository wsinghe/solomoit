@extends('layouts/app')

@section('content')
<div class="row">
    <div class="col-md-12">
        <h2>Edit Train - [{{ $train->name }}]</h2>
    </div>
</div>

@include('partials/msg')

<div class="table-responsive mt-3">
    <div class="card">
        <form method="post" action="{{route('train.update', [$train->id])}}">
            <div class="card-body">
                @csrf
                @method('PATCH')
                <div class="form-group">
                    <label for="name">Train Name</label>
                    <input type="text" id="name" name="name" class="form-control" value="{{$train->name}}">
                </div>
                <div class="form-group">
                    <label for="departure_datetime">Departure Date & Time</label>
                    <input type="datetime-local" id="departure_datetime" name="departure_datetime" class="form-control"  value="{{date('Y-m-d\TH:i', strtotime($train->departure_datetime))}}">
                </div>
                <div class="form-group">
                    <label for="seats">Departure Date & Time</label>
                    <input type="number" id="seats" name="seats" class="form-control" value="{{$train->seats}}">
                </div>
            </div>
            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Edit</button>
            </div>
        </form>
    </div>
</div>
@endsection
