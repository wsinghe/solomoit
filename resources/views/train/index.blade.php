@extends('layouts/app')

@section('content')
<div class="row">
    <div class="col-md-10">
        <h2>All Trains</h2>
    </div>
    <div class="col-md-2">
        <a type="button" class="btn btn-info" href="{{ route('train.create') }}">Create New Train</a>
    </div>
</div>

@include('partials/msg')

<div class="table-responsive mt-3">
    <table class="table table-striped table-sm">
        <thead>
        <tr>
            <th>#</th>
            <th>Train Name</th>
            <th>Departure Date & Time</th>
            <th>Available Seats</th>
            <th>Operation</th>
        </tr>
        </thead>
        <tbody>
        @foreach($trains as $train)
            <tr>
                <td>{{ $train->id }}</td>
                <td>{{ $train->name }}</td>
                <td>{{ $train->departure_datetime }}</td>
                <td>{{ $train->seats }}</td>
                <td>
                    <a type="button" class="btn btn-warning btn-sm" href="{{ route('train.edit', [$train->id]) }}">Edit</a>
                    <a type="button" class="btn btn-danger btn-sm ml-1" href="#">Delete</a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
@endsection
