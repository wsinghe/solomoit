<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\TrainController;
use App\Http\Controllers\HomeController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::resource('admin/train', TrainController::class);
Route::match(['get', 'post'], 'train/{id}', [App\Http\Controllers\HomeController::class, 'single'])->name('home.single');
Route::post('booking/confirm', [App\Http\Controllers\HomeController::class, 'book'])->name('home.book');
Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
