<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Train extends Model
{
    use HasFactory;

    public function getAvailableSeatAttribute()
    {
        $obj = Booking::select(DB::raw("SUM(seat_count) as seat_count"))->where('train_id', $this->id)->first();

        return ($this->seats - $obj->seat_count);
    }
}
