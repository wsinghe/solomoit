<?php

namespace App\Http\Controllers;

use App\Models\Booking;
use App\Models\Train;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $trains = Train::all();

        return view('home.index', compact('trains'));
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function single($id, Request $request)
    {
        if(isset($_POST) && !empty($_POST)) {
            $validatedData = $request->validate([
                'nic' => 'required|regex:/^\d{9}V$/',
                'seat_count' => 'required|integer'
            ]);

            $booking = new Booking();
            $booking->nic = $request->nic;
            $booking->seat_count = $request->seat_count;
            $booking->train_id = $request->train_id;

            $booking->save();

            return redirect('/')->with('success', 'Booking is completed Successfully. Your Booking ID : ' . $booking->id);
        } else {
            $train = Train::find($id);

            return view('home.single', compact('train'));
        }
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function book()
    {

    }
}
